//
//  ImageLoader.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import UIKit

class ImageLoader {
    
    static func loadImage(at url: URL, callback: @escaping (UIImage?) -> Void) {
        let dispatchedToMainThreadCallback = { input in
            DispatchQueue.main.async {
                callback(input)
            }
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil, let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode >= 200, httpResponse.statusCode < 300,
                let data = data else {
                dispatchedToMainThreadCallback(nil)
                return
            }
            
            dispatchedToMainThreadCallback(UIImage(data: data))
        }.resume()
    }
}
