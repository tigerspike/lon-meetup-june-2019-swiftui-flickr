//
//  FlickrAPIClient.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import UIKit

private struct FlickrMedia: Codable {
    let m: String
}

private struct FlickrItem: Codable {
    let title: String
    let media: FlickrMedia
}

private struct FlickrAPIResponse: Codable {
    let items: [FlickrItem]
}

class FlickrAPIClient: NSObject {
    
    static let flickrAPIURL = URL(string: "https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1&tags=doggo")!

    static func loadImage(callback: @escaping ([FlickrImage]?) -> Void) {
        let dispatchedToMainThreadCallback = { input in
            DispatchQueue.main.async {
                callback(input)
            }
        }
        
        URLSession.shared.dataTask(with: flickrAPIURL) { data, response, error in
            guard error == nil, let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode >= 200, httpResponse.statusCode < 300,
                let data = data else {
                    dispatchedToMainThreadCallback(nil)
                    return
            }
            
            do {
                let photos = try JSONDecoder().decode(FlickrAPIResponse.self, from: data)
                let flickrImages = photos.items.map {
                    FlickrImage(name: $0.title, url: URL(string: $0.media.m)!)
                }
                dispatchedToMainThreadCallback(flickrImages)
            } catch {
                dispatchedToMainThreadCallback(nil)
            }
        }.resume()
    }
}
