//
//  Store.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI
import Combine

struct FlickrImage: Identifiable {
    let id = UUID()
    let name: String
    let url: URL
    
    #if DEBUG
    static let testData = [
        FlickrImage(name: "Test data 1", url: URL(string: "https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ce2ece60-9b32-11e6-95ab-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg")!),
        FlickrImage(name: "Test data 2", url: URL(string: "https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ce2ece60-9b32-11e6-95ab-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg")!),
        FlickrImage(name: "Test data 3", url: URL(string: "https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ce2ece60-9b32-11e6-95ab-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg")!),
    ]
    #endif
}

enum FlickrState {
    case loading
    case loaded([FlickrImage])
    case error
    
    var isLoading: Bool {
        switch self {
        case .loading: return true
        default: return false
        }
    }
    
    var isError: Bool {
        switch self {
        case .error: return true
        default: return false
        }
    }
    
    var isLoaded: Bool {
        switch self {
        case .loaded(_): return true
        default: return false
        }
    }
    
    var loadedImages: [FlickrImage]? {
        switch self {
        case let .loaded(images): return images
        default: return nil
        }
    }
}

class Store: BindableObject {
    
    private(set) var state: FlickrState {
        didSet {
            didChange.send()
        }
    }

    var didChange = PassthroughSubject<Void, Never>()
    
    init() {
        state = .loading
        load()
    }
    
    func load() {
        state = .loading
        FlickrAPIClient.loadImage { images in
            guard let images = images else {
                self.state = .error
                return
            }
            
            self.state = .loaded(images)
        }
    }
}
