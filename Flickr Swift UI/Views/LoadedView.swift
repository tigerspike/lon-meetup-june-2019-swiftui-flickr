//
//  LoadedView.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI

struct LoadedView: View {
    let loadedImages: [FlickrImage]
    
    var body: some View {
        List(loadedImages) { image in
            NavigationButton(destination: DetailView(image: image)) {
                HStack {
                    RemoteImage(imageLocation: image.url)
                        .frame(minWidth: 50, maxWidth: 50, minHeight: 50, maxHeight: 50)
                        .clipped()
                    Text(image.name)
                }.padding(8)
            }
        }.navigationBarTitle(Text("Loaded images"))
    }
}

#if DEBUG
struct LoadedView_Previews : PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoadedView(loadedImages: FlickrImage.testData)
        }
    }
}
#endif
