//
//  ErrorView.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI

struct ErrorView: View  {
    let retry: () -> Void
    
    var body: some View {
        VStack {
            Text("An error occured, please try again.")

            Button(action: retry) {
                Text("Retry load")
            }.padding(.all)
        }.navigationBarTitle(Text("Error occured"), displayMode: .inline)
    }
}


#if DEBUG
struct ErrorView_Previews : PreviewProvider {
    static var previews: some View {
        NavigationView {
            ErrorView(retry: {})
        }
    }
}
#endif
