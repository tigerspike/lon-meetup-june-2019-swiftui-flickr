//
//  RemoteImage.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI
import Combine

enum ImageLoadState {
    case loading
    case loaded(UIImage)
    case error
    
    var isLoading: Bool {
        switch self {
        case .loading: return true
        default: return false
        }
    }
    
    var isError: Bool {
        switch self {
        case .error: return true
        default: return false
        }
    }
    
    var isLoaded: Bool {
        switch self {
        case .loaded(_): return true
        default: return false
        }
    }
    
    var loadedImage: UIImage? {
        switch self {
        case let .loaded(image): return image
        default: return nil
        }
    }
}

class ImageLoadStore: BindableObject {
    
    let imageLocation: URL
    let allowZooming: Bool
    var didChange = PassthroughSubject<Void, Never>()
    
    var zoomedIn: Bool = false {
        didSet {
            didChange.send()
        }
    }
    private(set) var state: ImageLoadState = .loading {
        didSet {
            didChange.send()
        }
    }
    
    init(imageLocation: URL, allowZooming: Bool) {
        self.imageLocation = imageLocation
        self.allowZooming = allowZooming
    }
    
    func load() {
        state = .loading
        ImageLoader.loadImage(at: imageLocation) { image in
            guard let image = image else {
                self.state = .error
                return
            }

            self.state = .loaded(image)
        }
    }
}

struct RemoteImage : View {

    @ObjectBinding
    var imageLoadStore: ImageLoadStore

    init(imageLocation: URL, allowZooming: Bool = false) {
        imageLoadStore = ImageLoadStore(imageLocation: imageLocation, allowZooming: allowZooming)
        imageLoadStore.load()
    }

    var body: some View {
        Group {
            if imageLoadStore.state.isLoading {
                Image(systemName: "arrow.counterclockwise.circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else if imageLoadStore.state.isError {
                Image(systemName: "wifi.exclamationmark")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else if imageLoadStore.state.isLoaded {
                Image(uiImage: imageLoadStore.state.loadedImage!)
                    .resizable()
                    .aspectRatio(contentMode: imageLoadStore.zoomedIn ? .fill : .fit)
                    .tapAction(count: 2) {
                        if self.imageLoadStore.allowZooming {
                            self.imageLoadStore.zoomedIn.toggle()
                        }
                    }
            }
        }
    }
}

#if DEBUG
struct RemoteImage_Previews : PreviewProvider {
    static var previews: some View {
        RemoteImage(imageLocation: URL(string: "https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ce2ece60-9b32-11e6-95ab-00163ed833e7/260663710/the-test-fun-for-friends-screenshot.jpg")!)
    }
}
#endif
