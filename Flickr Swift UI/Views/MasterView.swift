//
//  ContentView.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI

struct MasterView : View {

    @ObjectBinding
    var store: Store

    var body: some View {
        NavigationView {
            Group {
                if store.state.isLoading {
                    LoadingView()
                } else if store.state.isError {
                    ErrorView(retry: retry)
                } else if store.state.isLoaded {
                    LoadedView(loadedImages: store.state.loadedImages!)
                }
            }.navigationBarTitle(Text("SwiftUI Flickr"))
        }
    }
    
    func retry() {
        store.load()
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        MasterView(store: Store())
    }
}
#endif
