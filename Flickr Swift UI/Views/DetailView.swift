//
//  DetailView.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI

struct DetailView : View {
    let image: FlickrImage
    
    var body: some View {
        RemoteImage(imageLocation: image.url, allowZooming: true)
            .aspectRatio(contentMode: .fit)
    }
}

#if DEBUG
struct DetailView_Previews : PreviewProvider {
    static var previews: some View {
        DetailView(image: FlickrImage.testData[0])
    }
}
#endif
