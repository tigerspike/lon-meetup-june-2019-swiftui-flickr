//
//  LoadingView.swift
//  Flickr Swift UI
//
//  Created by Josep Rodriguez on 23/06/2019.
//  Copyright © 2019 Josep Rodriguez. All rights reserved.
//

import SwiftUI

struct LoadingView: View  {
    var body: some View {
        Text("Loading images...")
    }
}

#if DEBUG
struct LoadingView_Previews : PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoadingView()
        }
    }
}
#endif
